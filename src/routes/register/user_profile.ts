import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';
import { DateTime } from 'luxon';

import { UserProfile } from '../../models/register/user_profile';
import paramsSchema from '../../schema/register/user_profile';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

    const db = fastify.db;
    const userProfile = new UserProfile();

// Cretae By renfix UserProfile
  fastify.get('/info', {
    preHandler: [
      fastify.guard.role(['admin','nurse','doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: paramsSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const userId: any = request.user.sub;
      const result: any = await userProfile.info(db, userId);
      if (!_.isEmpty(result)) {
        return reply.status(StatusCodes.OK)
          .send(result);
      } else {
        return reply.status(StatusCodes.NOT_FOUND)
          .send({
            status: 'error',
            error: getReasonPhrase(StatusCodes.NOT_FOUND)
          });
      }

    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  done();

}
