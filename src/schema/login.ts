import S from 'fluent-json-schema'

const schema = S.object()
.prop('username', S.string().minLength(2).maxLength(13).required())
.prop('password', S.string().minLength(1).required())

export default {
  body: schema
}